package view;

import java.awt.EventQueue;

import javax.swing.JFrame;

import controller.AlljoynController;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import java.awt.Panel;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainView implements ActionListener{
	public JFrame _jFrame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {			
					MainView window = new MainView();
					window._jFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		_jFrame = new JFrame();
		_jFrame.setResizable(false);
		_jFrame.setBounds(100, 100, 450, 300);
		_jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		_jFrame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblTatapweekIdeias = new JLabel("TatapWeek Ideias");
		lblTatapweekIdeias.setFont(new Font("Tahoma", Font.PLAIN, 17));
		panel.add(lblTatapweekIdeias);
		
		Panel panel_3 = new Panel();
		_jFrame.getContentPane().add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnEnableGroup = new JButton("Habilitar cadastro de ideias");
		btnEnableGroup.addActionListener(this);
		btnEnableGroup.setName("btnEnable");
		
		panel_3.add(btnEnableGroup);
		
		JButton btnDisableGroup = new JButton("Desabilitar cadastro de ideias");
		btnDisableGroup.addActionListener(this);
		btnDisableGroup.setName("btnDisanabe");
		panel_3.add(btnDisableGroup);
		
		JButton btnRunWinners = new JButton("Apurar vencedores");
		btnRunWinners.addActionListener(this);
		btnRunWinners.setName("btnRunWinners");
		panel_3.add(btnRunWinners);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton jButton = (JButton)e.getSource();
		switch (jButton.getName()) {
		case "btnEnable":
			AlljoynController.getInstance().createGroup();
			break;
		case "btnDisanabe":
			AlljoynController.getInstance().lockGroup();
			break;
		case "btnRunWinners":
			break;

		default:
			break;
		}
		
	}

}
