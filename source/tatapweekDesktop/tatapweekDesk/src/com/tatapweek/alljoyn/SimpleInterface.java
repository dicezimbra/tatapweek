package com.tatapweek.alljoyn;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusSignal;

@BusInterface(name = "com.icc.qsa.pcsync")
public interface SimpleInterface {

	@BusSignal
	public void SendPing(String deviceName) throws BusException;
}
