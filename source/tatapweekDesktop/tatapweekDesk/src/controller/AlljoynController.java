package controller;

import java.util.ArrayList;

import org.alljoyn.bus.Status;
import org.alljoyn.cops.peergroupmanager.BusObjectData;
import org.alljoyn.cops.peergroupmanager.PeerGroupListener;
import org.alljoyn.cops.peergroupmanager.PeerGroupManager;

import com.tatapweek.alljoyn.SimpleService;

public class AlljoynController {

	private SimpleService _simpleService;
	private static AlljoynController _alljoynController;
	private PeerGroupManager _peerGroupManager;
	private final String _SERVICE_NAME = "com.tatapweek";
	private String _groupName = "TATAPWEEK";

	public static AlljoynController getInstance() {
		if (_alljoynController == null) {
			_alljoynController = new AlljoynController();
		}
		return _alljoynController;
	}

	public void createGroup() {
		_simpleService = new SimpleService();

		ArrayList<BusObjectData> busObjects = new ArrayList<BusObjectData>();
		busObjects.add(new BusObjectData(_simpleService, "/SimpleService"));

		PeerGroupListener pgListener = new PeerGroupListener() {
			@Override
			public void foundAdvertisedName(String groupName, short transport) {
				System.out.println("foundAdvertisedName");
			}

			@Override
			public void lostAdvertisedName(String groupName, short transport) {
				System.out.println("lostAdvertisedName");
			}

			@Override
			public void groupLost(String groupName) {
				System.out.println("groupLost");
			}

			@Override
			public void peerAdded(String peerId, String groupName, int numParticipants) {
				System.out.println("peerAdded");

			}

			@Override
			public void peerRemoved(String peerId, String groupName, int numPeers) {
				System.out.println("peerRemoved");
			}
		};

		_peerGroupManager = new PeerGroupManager(_SERVICE_NAME, pgListener, busObjects);

		Status status = _peerGroupManager.registerSignalHandlers(_simpleService);
		if (status == Status.OK) {
			System.out.println("REGISTER SIGNALS SUCESS!");
		} else {
			System.out.println("service_not_loaded");
		}
		status = _peerGroupManager.createGroup(_groupName);
		if (status == Status.OK) {
			System.out.println("CREATE GROUP SUCESS");
		} else {
			System.out.println("service_not_loaded");
		}
	}

	public void lockGroup() {
		Status status = _peerGroupManager.destroyGroup(_groupName);
		if (status == Status.OK) {
			System.out.println("Lock group SUCESS!");
		} else {
			System.out.println("Lock group Fail");
		}
		
	}

}
